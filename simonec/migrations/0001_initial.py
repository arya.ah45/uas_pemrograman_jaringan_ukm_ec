# Generated by Django 3.1.3 on 2020-12-22 05:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Riwayat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nim', models.CharField(max_length=10)),
                ('divisi', models.CharField(choices=[('welfare', 'welfare'), ('News Publication', 'News Publication'), ('Art And Sport', 'Art And Sport'), ('Advocation', 'Advocation'), ('Reasoning', 'Reasoning')], max_length=29)),
                ('tgl', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Tgl Edit')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-tgl'],
            },
        ),
        migrations.CreateModel(
            name='Anggota',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=150, verbose_name='Nama Anggota')),
                ('nim', models.CharField(max_length=10)),
                ('prodi', models.CharField(choices=[('mi', 'Manajemen Informatika'), ('tm', 'Teknik Mesin'), ('ak', 'Akuntansi')], max_length=2)),
                ('kelas', models.CharField(choices=[('1A', '1A'), ('1B', '1B'), ('1C', '1C'), ('1D', '1D'), ('1E', '1E'), ('2A', '2A'), ('2B', '2B'), ('2C', '2C'), ('2D', '2D'), ('2E', '2E'), ('3A', '3A'), ('3B', '3B'), ('3C', '3C'), ('3D', '3D'), ('3E', '3E')], max_length=2)),
                ('alamat', models.TextField()),
                ('email', models.CharField(max_length=50)),
                ('tgl', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Tgl Edit')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-tgl'],
            },
        ),
    ]
