from .models import Anggota , Riwayat
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import DetailView , CreateView , UpdateView , DeleteView
from .utils import Render
from django.views.generic import View

# Create your views here.
var = {
    'judul': 'SIMONEC',
    'HEAD': 'Manajemen Keanggotaan',
    'SUBHEAD': 'Sistem Informasi Keanggotaan Ec',
}


def index(self):
    var['anggota'] = Anggota.objects.values(
        'nama', 'nim', 'prodi', 'kelas', 'alamat', 'email','id').order_by('nama')
    return render(self, 'anggota/index.html', context=var)
def convert(list):
    return tuple(list) 

def indexr(self):
    var['riwayat'] = Riwayat.objects.raw('Select * from simonec_riwayat sr, simonec_anggota sa where sr.r_nim_id = sa.nim ')
    var['aa'] = Anggota.objects.raw('Select * from simonec_anggota ')
  
    ad =[]
    for a in var['aa']:
        ad.append((a.nim,a.nama))
    # var['chek'] = var['aa']
    var['jmlh'] = tuple(ad)
    return render(self, 'Riwayat/index.html',context=var) 
    # return render(self, 'Riwayat/index.html', context=var)
    # print(var)
class Detail_anggota(DetailView):
    model = Anggota
    template_name = 'anggota/detail_anggota.html'


class AnggotaCreateView(CreateView):
    model = Anggota
    fields = '__all__'
    template_name = "anggota/add_anggota.html"
    
    
class AnggotaUpdateView(UpdateView):
    model = Anggota
    fields = ['nama','kelas','prodi','nim','alamat','email']
    template_name = "anggota/edite_anggota.html"
    


class AnggotaDeleteView(DeleteView):
    model = Anggota
    template_name = "anggota/delete_anggota.html"
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var 
        context.update(super().get_context_data(**kwargs))
        return context

class cetakAnggota(View):
    def get(self,request):
        var = {
            'anggota' : Anggota.objects.values(
                'nim','nama','kelas','alamat','prodi','tgl','email'),
            'request':request
        }
        return Render.to_pdf(self,'anggota/cetak_anggota.html',var)

################################## riwayat

class RiwayatCreateView(CreateView):
    model = Riwayat
    fields = '__all__'
    template_name = "Riwayat/add_riwayat.html"
    

class RiwayatDeleteView(DeleteView):
    model = Riwayat
    template_name = "Riwayat/delete_riwayat.html"
    success_url = reverse_lazy('home_riwayat')

    def get_context_data(self, **kwargs):
        context = var 
        context.update(super().get_context_data(**kwargs))
        return context
    
class RiwayatUpdateView(UpdateView):
    model = Riwayat
    fields = ['r_nim_id','divisi','tgl','user']
    template_name = "Riwayat/edite_riwayat.html"



class cetakriwayat(View):
    def get(self,request):
        var = {
            'riwayat' : Riwayat.objects.values(
                'nama','nama','kelas','alamat','prodi','tgl','email'),
            'request':request
        }
        return Render.to_pdf(self,'riwayat/cetak_riwayat.html',var)