from django.urls import path
from .views import index  , cetakAnggota , indexr ,RiwayatUpdateView ,RiwayatDeleteView,RiwayatCreateView, AnggotaDeleteView ,Detail_anggota,AnggotaCreateView,AnggotaUpdateView


urlpatterns = [
    path('anggota/', index, name='home_page'),
    path('anggota/<int:pk>', Detail_anggota.as_view(),
         name='detail_anggota'),
    path('anggota/add/', AnggotaCreateView.as_view(),
         name='anggota_add'),
    path('anggota/edite/<int:pk>', AnggotaUpdateView.as_view(),
         name='edite_anggota'),
    path('anggota/delete/<int:pk>', AnggotaDeleteView.as_view(),
         name='delete_anggota'),
    path('anggota/cetak', cetakAnggota.as_view(), name='cetak_anggota'),
    path('riwayat/', indexr, name='home_riwayat'),
    path('riwayat/add/', RiwayatCreateView.as_view(),
         name='riwayat_add'),
    path('riwayat/delete/<int:pk>', RiwayatDeleteView.as_view(),
         name='delete_riwayat'),
    path('riwayat/edite/<int:pk>', RiwayatUpdateView.as_view(),
         name='edite_riwayat'),
      
]
