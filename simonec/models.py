from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse  # new
from django.utils import timezone
from django.db import connection
# Create your models here.


class Anggota(models.Model):
    PRODI_CHOICES = (
        ('mi', 'Manajemen Informatika'),
        ('tm', 'Teknik Mesin'),
        ('ak', 'Akuntansi'),
    )

    KELAS_CHOICES = (
        ('1A', '1A'),
        ('1B', '1B'),
        ('1C', '1C'),
        ('1D', '1D'),
        ('1E', '1E'),
        ('2A', '2A'),
        ('2B', '2B'),
        ('2C', '2C'),
        ('2D', '2D'),
        ('2E', '2E'),
        ('3A', '3A'),
        ('3B', '3B'),
        ('3C', '3C'),
        ('3D', '3D'),
        ('3E', '3E'),
    )
    nama = models.CharField('Nama Anggota', max_length=150, null=False)
    nim = models.CharField(max_length=10, null=False)
    prodi = models.CharField(max_length=2, choices=PRODI_CHOICES)
    kelas = models.CharField(max_length=2, choices=KELAS_CHOICES)
    alamat = models.TextField()
    email = models.CharField(max_length=50, null=False)
    tgl = models.DateTimeField('Tgl Edit', default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl']

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('home_page')


class Riwayat(models.Model):
    isi = Anggota.objects.raw('Select * from simonec_anggota ')
    ad =[]
    for a in isi:
        ad.append((a.nim,a.nama))
    # var['chek'] = var['aa']
    nim = tuple(ad)
    DIVISI_CHOICES = (
        ('welfare', 'welfare'),
        ('News Publication', 'News Publication'),
        ('Art And Sport', 'Art And Sport'),
        ('Advocation', 'Advocation'),
        ('Reasoning', 'Reasoning'),
    )

    r_nim_id = models.CharField(max_length=20,choices=nim)
    divisi = models.CharField(max_length=29, choices=DIVISI_CHOICES)
    tgl = models.DateTimeField('Tgl Edit', default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl']

    def __str__(self):
        return self.divisi

    def get_absolute_url(self):
        return reverse('home_riwayat')
