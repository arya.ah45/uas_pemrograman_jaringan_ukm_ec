from django.contrib import admin
from .models import Anggota
from .models import Riwayat


# Register your models here.
@admin.register(Anggota)
class AnggotaAdmin(admin.ModelAdmin):
    list_display = ['nama', 'nim', 'prodi',
                    'kelas', 'alamat', 'email', 'tgl', 'user']
    list_filter = ['nama', 'prodi', 'user']
    search_fields = ['nama', 'prodi', 'user']


@admin.register(Riwayat)
class RiwayatAdmin(admin.ModelAdmin):
    list_display = ['r_nim_id', 'divisi', 'tgl', 'user']
    list_filter = ['divisi', 'user']
    search_fields = ['r_nim_id', 'divisi', 'user']
